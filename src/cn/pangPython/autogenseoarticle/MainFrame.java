package cn.pangPython.autogenseoarticle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MainFrame extends JFrame {

	/**
	 * 主窗体文件，可以使用eclipse-windowbuilder 插件可视化编辑
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField title;
	private JTextField author;
	private JTextField url;
	private JTextField qrcodeAddr;
	private JTextField copyright;



	/**
	 * Create the frame.
	 * 使用绝对布局，简单些
	 */
	public MainFrame() {
		//设置jframe标题
		setTitle("SEO\u8F6F\u6587\u751F\u6210\u5668");
		//默认关闭方式
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//大小
		setBounds(100, 100, 704, 399);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 668, 340);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel jlb_title = new JLabel("\u7F51\u9875\u6807\u9898");
		jlb_title.setBounds(31, 23, 54, 15);
		panel.add(jlb_title);
		
		title = new JTextField();
		title.setBounds(101, 20, 66, 21);
		panel.add(title);
		title.setColumns(10);
		
		JLabel jlb_author = new JLabel("\u4F5C\u8005");
		jlb_author.setBounds(31, 67, 54, 15);
		panel.add(jlb_author);
		
		author = new JTextField();
		author.setBounds(101, 67, 66, 21);
		panel.add(author);
		author.setColumns(10);
		
		JLabel jlb_url = new JLabel("\u516C\u53F8\u5B98\u7F51");
		jlb_url.setBounds(31, 121, 54, 15);
		panel.add(jlb_url);
		
		url = new JTextField();
		url.setBounds(101, 118, 66, 21);
		panel.add(url);
		url.setColumns(10);
		
		JLabel jlb_qrCodeAddr= new JLabel("\u4E8C\u7EF4\u7801\u5730\u5740");
		jlb_qrCodeAddr.setBounds(31, 168, 66, 15);
		panel.add(jlb_qrCodeAddr);
		
		qrcodeAddr = new JTextField();
		qrcodeAddr.setBounds(101, 165, 66, 21);
		panel.add(qrcodeAddr);
		qrcodeAddr.setColumns(10);
		
		JLabel jlb_copyright = new JLabel("\u7248\u6743\u4E0E\u514D\u8D23");
		jlb_copyright.setBounds(31, 218, 66, 15);
		panel.add(jlb_copyright);
		
		copyright = new JTextField();
		copyright.setBounds(101, 218, 66, 21);
		panel.add(copyright);
		copyright.setColumns(10);
		
		JButton btn_gen = new JButton("\u5F00\u59CB\u751F\u6210");
		btn_gen.setBounds(297, 288, 93, 23);
		panel.add(btn_gen);
		
		
		
		JLabel jlb_content = new JLabel("\u6B63\u6587");
		jlb_content.setBounds(246, 23, 54, 15);
		panel.add(jlb_content);
		
		JTextArea content = new JTextArea();
		content.setBounds(297, 41, 327, 192);
		panel.add(content);
		
		//按钮绑定事件，点击按钮开始处理：读取html模板，替换内容，生成新html
		btn_gen.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Generator gen = new Generator();
				
				try {
					gen.createHTML(title.getText(), author.getText(),content.getText(), qrcodeAddr.getText(), copyright.getText(), url.getText());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//System.out.println("TXT"+title.getText()+" "+author.getText()+" "+url.getText()+" "+qrcodeAddr.getText()+" "+copyright.getText()+" "+content.getText());
			}
		});
	}
}
