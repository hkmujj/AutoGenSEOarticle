package cn.pangPython.autogenseoarticle;

import java.awt.EventQueue;

public class Main {
	public static void main(String[] args) {
		//把这个事件放入事件队列，并启动一个线程执行
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
