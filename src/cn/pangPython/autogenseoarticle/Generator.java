package cn.pangPython.autogenseoarticle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

public class Generator {
	//用于生成各种类型的文件
	//HTML
	//TXT暂未实现
	//Word暂未实现
	
	//生成软文单页
	public void createHTML(String title,String author,String content,String qrCodeAddr,String copyright,String url) throws IOException {
		FileWriter filewriter = null;
		PrintWriter printwriter = null;
		BufferedReader bufferedreader = null;
		String inline = null;
		String filename = "html/"+title+".html";
		try {
			//读取文件
			bufferedreader = readHTML("template/seo1.html");
			
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		}
		try {
				filewriter = new FileWriter(filename);
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		printwriter = new PrintWriter(filewriter);
		
		//遍历文件内容，并查找替换标题、作者、内容、二维码、超链接等
		while ((inline = bufferedreader.readLine())!=null) {
			if(inline.indexOf("{title}")>=0){
				inline = inline.replace("{title}", title);
			}
			if (inline.indexOf("{author}")>=0) {
				inline = inline.replace("{author}", author);
			}
			if(inline.indexOf("{content}")>=0){
				inline = inline.replace("{content}", content);
			}
			if(inline.indexOf("{qrCodeAddr}")>=0){
				inline = inline.replace("{qrCodeAddr}", qrCodeAddr);
			}
			if(inline.indexOf("{copyright}")>=0){
				inline = inline.replace("{copyright}", copyright);
			}
			if(inline.indexOf("{url}")>=0){
				inline = inline.replace("{url}", url);
			}
			printwriter.println(inline);
			
		}
		
		printwriter.close();
		filewriter.close();
		bufferedreader.close();	
		System.out.println(title+qrCodeAddr+copyright+url);
		JOptionPane.showMessageDialog(null,"网页已经生成："+filename, "提示",JOptionPane.CLOSED_OPTION);
	}
	
	//读取HTML模板文件,返回一个bufferedreader 
	public BufferedReader readHTML(String template_path) throws FileNotFoundException{
		FileReader filereader = null;
		BufferedReader bf_reader = null;
		try {
			filereader = new FileReader(template_path);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}

		 bf_reader = new BufferedReader(filereader);
		
		return bf_reader;
	}
	
	
}
